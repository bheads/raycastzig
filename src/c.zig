pub const sdl = @import("SDL.zig");


pub fn ptr(p: var) t: {
        const T = @typeOf(p);
            const info = @typeInfo(@typeOf(p)).Pointer;
                break :t if (info.is_const) ?[*]const info.child else ?[*]info.child;
} {
        return @ptrCast(@typeInfo(@typeOf(this)).Fn.return_type orelse unreachable, p);
}
