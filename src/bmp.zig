const std = @import("std");
const os = std.os;
const File = os.File;
const mem = std.mem;
const Allocator = mem.Allocator;
const warn = std.debug.warn;
const math = std.math;


pub const RGBA = packed struct {
    a: u8,
    b: u8,
    g: u8,
    r: u8,
};

pub const Color = packed union {
    z : RGBA,
    raw : u32,
};

const FileHeader = packed struct {
    bfType: [2]u8,
    bfSize: i32,
    bfReserved1: u16,
    bfReserved2: u16,
    bfOffsetBits: i32,
};

const ImageHeader = packed struct {
    biSize: i32, // should be >= 40
    biWidth: i32,
    biHeight: i32,
    biPlanes: i16, // must be 1
    biBitCount: i16, //bits per pixel
    biCompression: i32, 
    biSizeImage: i32, // may be zero
    biXPelsPerMeter: i32, 
    biYPelsPerMeter: i32,
    biClrUsed: i32,
    biClrImportant: i32,
};

const RGB = packed struct {
    b: u8,
    g: u8,
    r: u8,
};


fn abs(v: var) @typeOf(v) {
    return if (v < 0) -v else v;
}

pub const BMPImage = struct {
    const Self = this;

    width: usize,
    height: usize,

    alloc: *Allocator,
    file_name: []const u8, // incase you want to reload it?

    colors: []Color,

    pub fn free(self: *Self) void {
        self.alloc.free(self.colors);
    }

    /// Read in the contents of a BMP and returns a block of memory of the image from the given allocator
    pub fn load(alloc: *Allocator, file_name: []const u8) !BMPImage {
        // Step 1, read in the file contents
        var file = try File.openRead(alloc, file_name);
        defer file.close();

        // Get the length of the file
        const file_length: usize = try file.getEndPos();
        // make a buffer for the read
        var file_data: []u8 = try alloc.alloc(u8, file_length);
        defer alloc.free(file_data);
        
        // read the data
        const file_read_length: usize = try file.read(file_data);
        if (file_read_length != file_length) @panic("File Length and file read length did not match, TODO: support multi-read if this is and issue");

        // Step 2, load in the headers
        var file_header: *FileHeader = @ptrCast(*FileHeader, &file_data[0]);
        var image_header: *ImageHeader = @ptrCast(*ImageHeader, &file_data[@sizeOf(FileHeader)]);

        // Validate some bits in the header
        if (!mem.eql(u8, "BM", file_header.bfType)) return error.NotABitMapImageFile;
        if (file_header.bfOffsetBits < (@sizeOf(FileHeader) + @sizeOf(ImageHeader))) return error.NotABitMapImageFile; // normal offset will be 54 for 24bit images
        if (image_header.biSize < 40)  return error.NotABitMapImageFile; // Assume BMP v3 format
        if (image_header.biPlanes != 1)  return error.NotABitMapImageFile; // Must be 1
        if (image_header.biCompression != 0) return error.NonSupportedFormat; // no compression supported
        
        // image information
        const bottom_up: bool = !(image_header.biHeight > 0);
        const height: usize = @intCast(usize, abs(image_header.biHeight));
        const width: usize = @intCast(usize, abs(image_header.biWidth));

        // Step 3, make an output buffer
        
        var image: []Color = try alloc.alloc(Color, width * height);
        warn("Width: {}  Height: {}   {}\n", width, height, image.len);
        errdefer alloc.free(image); // clean up on errors

        // Step 4, Read in the data

        // compute the scan line length, data is 4 byte alligned
        const scan_length: usize = @floatToInt(usize, math.floor(@intToFloat(f64, @intCast(usize, image_header.biBitCount) * width + 31) / 32.0) * 4.0);
        std.debug.assert(@floatToInt(usize, @intToFloat(f64, scan_length) * @intToFloat(f64, height) + @intToFloat(f64, file_header.bfOffsetBits)) == file_length); // the scan_line * height + file headers should equal the whole file
        switch (image_header.biBitCount) {
            24 => {
                // comes in rgb u24 bit blocks          
                var y: usize = 0;            
                var start = @intCast(usize, file_header.bfOffsetBits);
                while (y < height) : (y += 1) {
                    // read the colors
                    var x: usize = 0;
                    while (x < width) : (x += 1) {
                        var src_color: *RGB = @ptrCast(*RGB, &file_data[start + (scan_length * y) + (x * 3)]);
                        var dest_color: *RGBA = if (bottom_up) &image[((height - y - 1) * height) + x].z else  &image[(y * height) + x].z;
                        
                        dest_color.r = src_color.r;
                        dest_color.g = src_color.g;
                        dest_color.b = src_color.b;                        
                    }
                }
            },
            else => return error.NonSupportedFormat, // Only support true color bmp
        }

        return BMPImage {
            .width = width,
            .height = height,

            .alloc = alloc,
            .file_name = file_name,

            .colors = image,
        };
    }

};
