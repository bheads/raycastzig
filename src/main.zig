const std = @import("std");
const warn = std.debug.warn;
const C = @import("c.zig");
const sdl = C.sdl;
const bmp = @import("bmp.zig");
const BMPImage = bmp.BMPImage;
const Color = bmp.Color;
const RGBA = bmp.RGBA;
const math = std.math;
const os = std.os;
const Timer = os.time.Timer;
const fmt = std.fmt;

const width : i32 = 896;
const height : i32 = 504;
const half_height: i32 = height / 2;

fn Point(comptime T: type) type {
    return struct {
        x: T,
        y: T,
    };
}

const Player = struct {
    pos: Point(f64),
    dir: Point(f64),
    plane: Point(f64),

    time: f64,
    old_time: f64,
};

const world = [24][24]u8 {
    [24]u8{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,2,2,2,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
    [24]u8{1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,3,0,0,0,3,0,0,0,1},
    [24]u8{1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,2,2,0,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,0,0,0,0,5,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    [24]u8{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
};


var fixed_buffer_allocator_memory: [500000 * @sizeOf(usize)]u8 = undefined; // big block of memory, not initilized
pub fn main() void {
    var fixed_buffer_allocator = std.heap.FixedBufferAllocator.init(fixed_buffer_allocator_memory[0..]);
    warn("Raycazig\n");

    // Load in the textures
    //TODO: hard coded sucks    
    var brick1: BMPImage = BMPImage.load(&fixed_buffer_allocator.allocator, "C:\\Projects\\raycastzig\\data\\tex\\stone1.bmp") catch |err| {
        warn("Failed to load brick1.bmp {s}\n", err);
        std.os.exit(1);
    };
    defer brick1.free();
    var textures = [1]*BMPImage{ &brick1 };


    defer warn("Shutting Down Raycazig 1.0a\n");
    if (sdl.SDL_Init(sdl.SDL_INIT_VIDEO | sdl.SDL_INIT_EVENTS) != 0) {
        warn("SDL_Init error {s}", sdl.SDL_GetError());
        std.os.exit(1);
    }
    defer sdl.SDL_Quit();

    var linked : sdl.SDL_version = undefined;
    sdl.SDL_GetVersion(C.ptr(&linked));
    warn("SDL {}.{}.{}\n", linked.major, linked.minor, linked.patch);


    // Create the window
    const screen = Point(i32) { .x = 1280, .y = 720 };
    var window : *sdl.SDL_Window = sdl.SDL_CreateWindow(c"Raycaszig", 0x2FFF0000, 0x2FFF0000, screen.x, screen.y, sdl.SDL_WINDOW_ALLOW_HIGHDPI) orelse  {
        warn("SDL_CreateWindow error {s}\n", sdl.SDL_GetError());
        std.os.exit(1);
    };
    defer sdl.SDL_DestroyWindow(window);

    // Create the render
    var renderer : *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, sdl.SDL_RENDERER_ACCELERATED) orelse {
        warn("SDL_CreateRenederer error {s}\n", sdl.SDL_GetError());
        std.os.exit(1);
    };
    defer sdl.SDL_DestroyRenderer(renderer);

    // Create the buffer texture
    var rtexture : *sdl.SDL_Texture = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_RGBA8888, sdl.SDL_TEXTUREACCESS_STREAMING, height, width) orelse {
        warn("SDL_CreateTexture error {s}\n", sdl.SDL_GetError());
        std.os.exit(1);
    };
    defer sdl.SDL_DestroyTexture(rtexture);

    var buffer : []Color = fixed_buffer_allocator.allocator.alloc(Color, @intCast(usize, width * height)) catch {
        warn("Failed to allocate render buffer\n");
        std.os.exit(1);
    };
    defer fixed_buffer_allocator.allocator.free(buffer);

    var player = Player {
        .pos = Point(f64){ .x = 22, .y = 12},
        .dir = Point(f64){ .x = -1, .y = 0},
        .plane = Point(f64){ .x = 0, .y = 0.66},

        .time = 0,
        .old_time = 0,
    };

    // Render loop
    var event : sdl.SDL_Event = undefined;
    var running = true;
    var frame_timer = Timer.start() catch |err| {
        warn("Failed to start the frame timer, due to: {}\n", err);
        os.exit(1);
    };
    var frame_count: i32 = 0;
    var fps: i32 = 0;
    var fps_time: f64 = 0;

    while(running) {
        // get the frame time (should be close to zero the first time!
        var frame_delta: f64 = @intToFloat(f64, frame_timer.lap()) / @intToFloat(f64, os.time.ns_per_s);
        frame_count += 1;
        fps_time += frame_delta;
        if (fps_time >= 1.0) {
            fps_time -= 1.0;
            fps = frame_count;
            frame_count = 0;
            // log fps
            var window_title = fmt.allocPrint(&fixed_buffer_allocator.allocator, "RayCastzig  FPS: {}  Delta: {.6}\x00", fps, frame_delta) catch unreachable;
            defer fixed_buffer_allocator.allocator.free(window_title);
            sdl.SDL_SetWindowTitle(window, C.ptr(&window_title[0]));
        }


         // Clear screen
         _ = sdl.SDL_RenderClear(renderer);

        // Process input
         while (sdl.SDL_PollEvent(C.ptr(&event)) == 1) {
            if(event.type == sdl.SDL_QUIT) {
                running = false;
            }
        }

        playerInput(&player, frame_delta);

        // Render
        raycast(buffer, &player, textures[0..]);

        _ = sdl.SDL_UpdateTexture(rtexture, null, @ptrCast(?*const c_void, &buffer[0]), height * 4);

        var dest_rect = sdl.SDL_Rect{.x = 0, .y = screen.y, .w = screen.y, .h = screen.x};
        var rot_point = sdl.SDL_Point{.x = 0, .y = 0};
        _ = sdl.SDL_RenderCopyEx(renderer, rtexture, null, C.ptr(&dest_rect), -90.0, C.ptr(&rot_point), sdl.SDL_RendererFlip.SDL_FLIP_NONE);

        // Present
        sdl.SDL_RenderPresent(renderer);
    }
}


fn playerInput(p: *Player, frame_delta: f64) void {
    // Get key input
    var sdl_keys = sdl.SDL_GetKeyboardState(null);
    if (sdl_keys) |keys| {
        const move_speed: f64 = frame_delta * 5.0;
        const rot_speed: f64 = frame_delta * 3.0;

        if (keys[sdl.SDL_SCANCODE_UP] == 1) {
            if (world[@floatToInt(usize, math.round(p.pos.x + p.dir.x * move_speed))][@floatToInt(usize, p.pos.y)] == 0) p.pos.x += p.dir.x * move_speed;
            if (world[@floatToInt(usize, p.pos.x)][@floatToInt(usize, math.round(p.pos.y + p.dir.y * move_speed))] == 0) p.pos.y += p.dir.y * move_speed;
        }
        if (keys[sdl.SDL_SCANCODE_DOWN] == 1) {
            if (world[@floatToInt(usize, math.round(p.pos.x - p.dir.x * move_speed))][@floatToInt(usize, p.pos.y)] == 0) p.pos.x -= p.dir.x * move_speed;
            if (world[@floatToInt(usize, p.pos.x)][@floatToInt(usize, math.round(p.pos.y - p.dir.y * move_speed))] == 0) p.pos.y -= p.dir.y * move_speed;
        }
        if (keys[sdl.SDL_SCANCODE_RIGHT] == 1) {
            var dir_x = p.dir.x;
            p.dir.x = p.dir.x * math.cos(-rot_speed) + p.dir.y * -math.sin(-rot_speed);
            p.dir.y = dir_x * math.sin(-rot_speed) + p.dir.y * math.cos(-rot_speed);
            var plane_x = p.plane.x;
            p.plane.x = p.plane.x * math.cos(-rot_speed) + p.plane.y * -math.sin(-rot_speed);
            p.plane.y = plane_x * math.sin(-rot_speed) + p.plane.y * math.cos(-rot_speed);
        }
        if (keys[sdl.SDL_SCANCODE_LEFT] == 1) {
            var dir_x = p.dir.x;
            p.dir.x = p.dir.x * math.cos(rot_speed) + p.dir.y * -math.sin(rot_speed);
            p.dir.y = dir_x * math.sin(rot_speed) + p.dir.y * math.cos(rot_speed);
            var plane_x = p.plane.x;
            p.plane.x = p.plane.x * math.cos(rot_speed) + p.plane.y * -math.sin(rot_speed);
            p.plane.y = plane_x * math.sin(rot_speed) + p.plane.y * math.cos(rot_speed);
        }
    }
}


fn raycast(buffer: []Color, p: *Player, textures: []*BMPImage) void {
    for(buffer) |*c, i| {
        if (i % @intCast(usize, height) < @intCast(usize, half_height)) {
            c.z.r = 98;
            c.z.g = 98;
            c.z.b = 98;
        } else {
            c.z.r = 68;
            c.z.g = 68;
            c.z.b = 68;
        }
    }

    var x: i32 = 0;
    while(x < width) : (x += 1) {
        var camera_x: f64 = 2.0 * @intToFloat(f64, x) / @intToFloat(f64, width) - 1.0;
        //warn("x = {}, camera_x = {}\n", x, camera_x);
        var ray = Point(f64){
            .x = p.dir.x + p.plane.x * camera_x,
            .y = p.dir.y + p.plane.y * camera_x,
        };
        if (ray.x == 0.0) ray.x = 0.0000000000000001;
        if (ray.y == 0.0) ray.y = 0.0000000000000001;

        var map = Point(i32) {.x = @floatToInt(i32, p.pos.x), .y = @floatToInt(i32, p.pos.y)};
        var side_dist = Point(f64) {.x = 0, .y = 0};

        var delta_dist = Point(f64) {.x = math.fabs(1.0/ray.x), .y = math.fabs(1.0/ray.y)};
        var wall_dist: f64 = 0;
        var step = Point(i32) { .x = 0, .y = 0 };
        var hit: i32 = 0;
        var side: i32 = 0;

        if (ray.x < 0) {
            step.x = -1;
            side_dist.x = (p.pos.x - @intToFloat(f64, map.x)) * delta_dist.x;
        } else {
            step.x = 1;
            side_dist.x = (@intToFloat(f64, map.x + 1) - p.pos.x) * delta_dist.x;
        }

        if (ray.y < 0) {
            step.y = -1;
            side_dist.y = (p.pos.y - @intToFloat(f64,  map.y)) * delta_dist.y;
        } else {
            step.y = 1;
            side_dist.y = (@intToFloat(f64, map.y + 1) - p.pos.y) * delta_dist.y;
        }

        while(hit == 0) {
            if (side_dist.x < side_dist.y) {
                side_dist.x += delta_dist.x;
                map.x += step.x;
                side = 0; // TODO: convert side into an enum
            } else {
                side_dist.y += delta_dist.y;
                map.y += step.y;
                side = 1; // TODO: convert side into an enum
            }

            if (map.x < 0 or @intCast(usize, map.x) >= world.len) break;
            if (map.y < 0 or @intCast(usize, map.y) >= world[0].len) break;
            if (world[@intCast(usize, map.x)][@intCast(usize, map.y)] > 0) hit = 1;
        }

        // Got a hit on an object
        // find the distance to the wall
        wall_dist = if (side == 0) (@intToFloat(f64, map.x) - p.pos.x + (1.0 - @intToFloat(f64, step.x)) / 2.0) / ray.x 
            else (@intToFloat(f64, map.y) - p.pos.y + (1.0 - @intToFloat(f64, step.y)) / 2.0) / ray.y;

        // compute the height of the wall, centered on the screen
        var line_height: i32 = @floatToInt(i32, (@intToFloat(f64, height) / wall_dist) / 2.0);
        var draw_start: i32 = half_height - line_height;
        var draw_end: i32 = half_height + line_height;

        // pick the color
        var tex_index: u8 = 0; //world[@intCast(usize, map.x)][@intCast(usize, map.y)]; // TODO: more textures and fix map index values
        var tex = textures[tex_index];
        
        // Find where on the wall was it hit
        var wall_x: f64 = 0;
        if (side == 0) wall_x = p.pos.y + wall_dist * ray.y;
        if (side == 1) wall_x = p.pos.x + wall_dist * ray.x;
        wall_x -= math.floor(wall_x); // percent along the wall

        var tex_x: i32 = @floatToInt(i32, wall_x * @intToFloat(f64, tex.width)); // x cord on the texture
        // flip the texture x cord if the opposite side
        if (side == 0 and ray.x > 0) tex_x = @floatToInt(i32, tex.width) - tex_x - 1;
        if (side == 1 and ray.y < 0) tex_x = @floatToInt(i32, tex.width) - tex_x - 1;

        line_height *= 2;

        var y: i32 = draw_start;
        while (y < draw_end) : (y += 1) {
            if (y < 0 or y >= height) continue;

            var tex_y: i32 = @floatToInt(i32, (@intToFloat(f64, y - draw_start) / @intToFloat(f64, line_height)) * @intToFloat(f64, tex.height));
            if (tex_x < 0) tex_x = 0;
            if (tex_x >= @floatToInt(i32, tex.width)) tex_x = @floatToInt(i32, tex.width) - 1;
            if (tex_y < 0) tex_y = 0;
            if (tex_y >= @floatToInt(i32, tex.height)) tex_y = @floatToInt(i32, tex.height) - 1;
            
            var color = tex.colors[@intCast(usize, tex_y) * tex.width + @intCast(usize, tex_x)];
            if (side == 1)  {
                color.z.r /= 2;
                color.z.g /= 2;
                color.z.b /= 2;
            }

            var i = @intCast(usize, (x * height) + y);
            if (i < buffer.len) buffer[i].raw = color.raw;
        }        
    }
}








