const std = @import("std");
const os = std.os;
const Builder = std.build.Builder;
const builtin = @import("builtin");

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();
    const thisPath = os.selfExePath(std.debug.global_allocator);

    switch (builtin.os) {
        builtin.Os.linux => {
            b.addLibPath("/usr/lib/x86_64-linux-gnu");
            b.addCIncludePath("/usr/include/SDL2");
        },
        builtin.Os.windows => {
            b.addLibPath(".\\lib-win64\\");
            b.addCIncludePath(".\\lib-win64\\include");            
        }, 
        else => @compileError("TODO: Support this platform"),
    }    

    var exe = b.addExecutable("raycastzig", "src/main.zig");
    exe.setBuildMode(mode);

    switch (builtin.os) {
        builtin.Os.linux => {
            exe.linkSystemLibrary("SDL2");
        },
        builtin.Os.windows => {
            exe.linkSystemLibrary("SDL2.lib");            
        }, 
        else => @compileError("TODO: Support this platform"),
    }

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);

    const play = b.step("run", "Run the raycaster");
    const run = b.addCommand(".", b.env_map, [][]const u8{exe.getOutputPath()});
    play.dependOn(&run.step);
    run.step.dependOn(&exe.step);
}

